# chaveiro

Este projeto é um protótipo de chaveiro para a ferramenta de assinatura
digital `signify`.

O programa [signify](https://www.openbsd.org/papers/bsdcan-signify.html)
usa criptografia assimétrica para assinatura digital (originalmente
dentro do projeto [OpenBSD](https://www.openbsd.org/)).

Este chaveiro, portanto, nada mais é que um mecanismo para gerenciar as
chaves (suas próprias chaves, ou chaves públicas obtidas de terceiros) a
serem usadas pelo programa `signify`.

Caso tenha dúvidas sobre o projeto, veja se já não foram respondidas na
lista de [dúvidas potencialmente
frequentes](https://codeberg.org/hrcerq/chaveiro/wiki/Duvidas).

## Instruções

Se já não o tiver feito, crie e seu repositório local para Perl. Por
exemplo:

```shell
mkdir -p $HOME/.perl/bin
mkdir -p $HOME/.perl/lib/perl5
mkdir -p $HOME/.perl/man/man1
```

Configure as variáveis de ambiente aplicáveis a esse repositório. Por
exemplo (lembre-se de adaptar os valores ao seu ambiente):

```shell
PATH="$HOME/.perl/bin${PATH:+:${PATH}}";
PERL5LIB="$HOME/.perl/lib/perl5${PERL5LIB:+:${PERL5LIB}}";
PERL_LOCAL_LIB_ROOT="$HOME/.perl${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}";
PERL_MB_OPT="--install_base \"$HOME/.perl\"";
PERL_MM_OPT="INSTALL_BASE=$HOME/.perl";
MANPATH="$MANPATH:$HOME/.perl/man"

export PERL5LIB PERL_LOCAL_LIB_ROOT PERL_MB_OPT PERL_MM_OPT
export PATH MANPATH
```

Por fim, adicione o arquivo `bin/chaveiro` a `$HOME/.perl/bin`, e o
arquivo `man/man1/chaveiro.1p` a `$HOME/.perl/man/man1`.

Uma vez instalado, consulte o menu de ajuda ou leia o manual do
chaveiro.

```shell
chaveiro ajuda
man 1p chaveiro
```

Para instruções mais detalhadas, consulte a
[wiki](https://codeberg.org/hrcerq/chaveiro/wiki).

## Isto é apenas um protótipo

Quando decidi criar este chaveiro, não tinha nenhuma pretensão de que
ele fosse incorporado ao repositório de um sistema operacional, ou que
fosse usado no dia a dia por usuários de sistemas do tipo UNIX ou
semelhantes.

A ideia aqui é apenas provar uma possibilidade, esboçar o que
futuramente pode ser uma solução real para questões relacionadas à
confiança.

Inicialmente, escrevi em Shell Script, por ser uma excelente opção para
prototipar programas. No entanto, à medida que decidi implementar alguns
controles e tratamentos, bem como melhorar a documentação, achei que
seria mais interessante migrar o código para Perl.

O programa ainda carece de muitos tratamentos de erros, mas já ficou
mais robusto que a versão anterior. A versão em Shell não foi removida,
apenas movida para o diretório `legado`.

Roteiros de testes ainda precisam ser escritos também. Por enquanto,
foram feitos manualmente apenas.
