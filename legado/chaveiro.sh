#!/bin/sh

# chaveiro.sh - gerenciador de chaves do OpenBSD Signify
#
# Gerencia chaves criptográficas para a ferramenta de assinatura signify
# (implementação de referência: OpenBSD signify).
#
# Autor:    Hugo R. Cerqueira
# Licença:  FreeBSD (BSD simplificada) | vide arquivo LICENSE

VERSAO=0.1.0

DIR_CHAVEIRO="$HOME/.chaveiro_signify"
REGISTRO="registro"

COMANDOS="
Subcomandos disponíveis:

    ajuda - mostra essa mensagem
    vchav - verifica pela presença de um chaveiro
    cchav - cria um novo chaveiro, caso já não haja um
    lchav - lista as chaves presentes no chaveiro
    gpar  - gera um novo par de chaves
    ichav - importa uma chave (adiciona ao chaveiro)
            obs: a chave deve ser informada como parâmetro
    rchav - remove uma chave
    valid - valida uma chave presente no chaveiro
            obs: o identificador da chave deve ser informado como parâmetro"

AJUDA="
chaveiro - gerenciador de chaves do OpenBSD Signify
Versão $VERSAO
$COMANDOS"

SIGNIFY=0
ALERTA_SIGNIFY="
ATENÇÃO: o programa signify não está instalado no seu sistema, ou não está
mapeado para nenhum diretório da variável PATH. O comando de geração de chaves
não funcionará.
"

# Checagem inicial
which signify > /dev/null 2> /dev/null
if test "$?" -eq 0; then
    SIGNIFY=1
fi

# Se o signify não estiver disponível, emitir alerta
if test "$SIGNIFY" -eq 0; then
    printf "%s\n" "$ALERTA_SIGNIFY"
fi

# ajuda
#
# Exibe nome do programa e comandos disponíveis.
ajuda() {
    printf "%s\n" "${AJUDA}"
}

# verificar_chaveiro
#
# Verifica pela presença de um chaveiro e notifica sobre eventuais
# inconsistências, se aplicável.
verificar_chaveiro() {
    for i in                    \
        "$DIR_CHAVEIRO"         \
        "$DIR_CHAVEIRO/pub"     \
        "$DIR_CHAVEIRO/sec"     \
        "$DIR_CHAVEIRO/del"
    do
        test -d "$i" || printf "%s não existe.\n" "$i"
    done
    test -f "$DIR_CHAVEIRO/$REGISTRO" || printf "Registro não existe\n"
}

# criar_chaveiro
#
# Gera um novo chaveiro, caso já não exista um, ou cria partes que faltarem,
# se alguma parte do chaveiro estiver faltando.
criar_chaveiro() {
    for subdir in pub sec del
    do
        mkdir -p "$DIR_CHAVEIRO/$subdir"
    done
    find "$DIR_CHAVEIRO" -type d -exec chmod 700 {} \;
    test -f "$DIR_CHAVEIRO/$REGISTRO" || touch "$DIR_CHAVEIRO/$REGISTRO"
    find "$DIR_CHAVEIRO" -type f -exec chmod 600 {} \;
}

# listar_chaves
#
# Lista as chaves presentes no chaveiro.
listar_chaves() {
    awk -F ":" \
        'BEGIN {
            tipo[0] = "Privada"
            tipo[1] = "Pública"
            valida[0] = "Não"
            valida[1] = "Sim"
            printf("%-10s\t%-10s\t%-10s\t%-20s\n",
                "Id", "Tipo", "Válida", "Descrição")
            printf("%-10s\t%-10s\t%-10s\t%-20s\n",
                "--", "----", "------", "---------")
        }
        {
            printf ("%-10s\t%-10s\t%-10s\t%-20s\n",
                $3, tipo[$1], valida[$2], $4)
        } ' \
        "$DIR_CHAVEIRO/$REGISTRO"
}

# gerar_novo_par
#
# Gera um novo par de chaves e adiciona ao registro do chaveiro. Por serem
# criadas pelo próprio dono do chaveiro, são sempre consideradas válidas.
gerar_novo_par() {
    if test "$SIGNIFY" -eq 0; then
        printf "Comando signify não disponível. Não é possível prosseguir.\n"
        exit 1
    fi
    printf "Identificador da chave: "
    read -r id_chave
    test -z "$id_chave" && exit 1

    printf "Comentário da chave pública: "
    read -r comentario
    test -z "$comentario" && exit 1

    signify -G \
        -c "$comentario" \
        -p "$DIR_CHAVEIRO/pub/${id_chave}.pub" \
        -s "$DIR_CHAVEIRO/sec/${id_chave}.sec"

    # 0 - chave privada / 1 - validada
    printf "%d:%d:%s:%s\n" 0 1 \
        "$id_chave" "$comentario" >> "$DIR_CHAVEIRO/$REGISTRO"
    # 1 - chave pública / 1 - validada
    printf "%d:%d:%s:%s\n" 1 1 \
        "$id_chave" "$comentario" >> "$DIR_CHAVEIRO/$REGISTRO"
}

# importar_chave
#
# Importa uma chave pública ou privada, gerada por processo externo a esse
# chaveiro (ainda que tenha sido gerada por outro chaveiro igual a esse).
#
# Chaves públicas importadas são sempre consideradas como não validadas. Devem
# passar por processo de validação posterior. Chaves privadas, por outro lado,
# são sempre válidas, uma vez que não faz sentido fraudá-las.
importar_chave() {
    nova_chave="$1"
    test -f "$nova_chave" || exit 1
    test -z "$nova_chave" && exit 1
    printf "Identificador da chave: "
    read -r id_chave
    meta_chave="$(sed -n \
        's/^untrusted comment: \(.*\) \(secret\|public\) key$/\1:\2/p' \
        "$nova_chave")"
    tipo_chave="$(echo "$meta_chave" | cut -d : -f 2)"
    comentario="$(echo "$meta_chave" | cut -d : -f 1)"
    case "$tipo_chave" in
        "secret")
            # 0 - chave privada / 1 - validada
            printf "%d:%d:%s:%s\n" 0 0 \
                "$id_chave" "$comentario" >> "$DIR_CHAVEIRO/$REGISTRO"
            cp "$nova_chave" "$DIR_CHAVEIRO/sec/${id_chave}.sec"
            ;;
        "public")
            # 1 - chave pública / 0 - não validada
            printf "%d:%d:%s:%s\n" 1 0 \
                "$id_chave" "$comentario" >> "$DIR_CHAVEIRO/$REGISTRO"
            cp "$nova_chave" "$DIR_CHAVEIRO/pub/${id_chave}.pub"
            ;;
        *)
            printf "Tipo de chave inválido\n" && exit 1
            ;;
    esac
}

# remover_chave
#
# Remove uma chave, pública ou privada, bem como sua entrada no registro.
remover_chave() {
    # Cria cópia de segurança do registro
    cp "$DIR_CHAVEIRO/$REGISTRO" "$DIR_CHAVEIRO/$REGISTRO~"

    printf "Tipo de chave (0=privada, 1=pública): "
    read -r tipo_chave
    test -z "$tipo_chave" && exit 1
    printf "Identificador da chave: "
    read -r id_chave
    test -z "$id_chave" && exit 1
    awk -F ":"          \
        -v TIPO="$tipo_chave"    \
        -v IDENT="$id_chave"   \
        '!($1 ~ TIPO && $3 ~ IDENT) {print}' \
        "$DIR_CHAVEIRO/$REGISTRO~" > "$DIR_CHAVEIRO/$REGISTRO"

    # Mover a chave para diretório de chaves não gerenciadas
    if test "$tipo_chave" -eq 1; then
        mv "$DIR_CHAVEIRO/pub/${id_chave}.pub" "$DIR_CHAVEIRO/del/"
    else
        mv "$DIR_CHAVEIRO/sec/${id_chave}.sec" "$DIR_CHAVEIRO/del/"
    fi
}

# validar
#
# Valida autenticidade de chave pública, por meio de mecanismo considerado
# confiável. Por enquanto o único mecanismo suportado é a validação manual, ou
# seja, o usuário deliberadamente define uma chave como confiável.
validar() {
    # Cria cópia de segurança do registro
    cp "$DIR_CHAVEIRO/$REGISTRO" "$DIR_CHAVEIRO/$REGISTRO~"

    id_chave="$1"
    test -z "$id_chave" && printf "Nenhuma chave informada\n" && exit 1
    cut -d ":" -f 3 "$DIR_CHAVEIRO/$REGISTRO" | grep -E "^$id_chave$"   \
        > /dev/null
    if test "$?" -ne 0; then
        printf "Nenhuma chave com identificador %s foi encontrada.\n"   \
        "$id_chave"
        exit 1
    fi
    awk -F ":"          \
        -v IDENT="$id_chave"   \
        '$3 ~ IDENT {
            printf("%d:%d:%s:%s\n",$1,1,$3,$4)
            next
        }
        {print}' \
        "$DIR_CHAVEIRO/$REGISTRO~" > "$DIR_CHAVEIRO/$REGISTRO"
}

# Execução de subcomando

case "$1" in
    ajuda)
        ajuda
        ;;
    vchav)
        verificar_chaveiro
        ;;
    cchav)
        criar_chaveiro
        ;;
    lchav)
        listar_chaves
        ;;
    gpar)
        gerar_novo_par
        ;;
    ichav)
        importar_chave "$2"
        ;;
    rchav)
        remover_chave
        ;;
    valid)
        validar "$2"
        ;;
    *)
        printf "%s\n" "${COMANDOS}"
        ;;
esac
